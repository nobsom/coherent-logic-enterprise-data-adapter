package com.coherentlogic.coherent.data.adapter.core.plugins;

import java.util.Optional;

import org.springframework.beans.factory.config.BeanDefinition;

/**
 * Inspired in part by the Grails plugin architecture.
 *
 * @author Thomas P. Fuller
 *
 */
public interface PluginSpecification {

    Optional<String> getVersion ();

    Optional<String> getAuthor ();

    Optional<String> getEmailAddress ();

    /**
     * The plugin name is mandatory and must be unique.
     */
    Optional<String> getPluginName ();

    Optional<String> getDescription ();

    Optional<String> getDocumentation ();

    /**
     * Method returns the bean definition for this plugin. This method must return a non-null reference.
     *
     * @see {@link BeanDefinition}
     */
    Optional<BeanDefinition> getBeanDefinition ();
}
