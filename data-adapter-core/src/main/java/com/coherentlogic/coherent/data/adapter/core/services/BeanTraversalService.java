package com.coherentlogic.coherent.data.adapter.core.services;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;

import org.springframework.util.ReflectionUtils;
import org.springframework.util.ReflectionUtils.FieldCallback;
import org.springframework.util.ReflectionUtils.FieldFilter;

import com.coherentlogic.coherent.data.adapter.core.exceptions.GenericReflectionException;
import com.coherentlogic.coherent.data.adapter.core.exceptions.InvalidConfigurationException;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * Used to recursively scan every property in a bean that is marked with the {@link Visitable} annotation.
 *
 * Note that List objects will be inspected and if the contents are instances of {@link SerializableBean} then these
 * will be iterated over as well (this is not the case for an array, however).
 *
 * @deprecated This class is no longer being used.
 */
public class BeanTraversalService<R> {

    private Map<Class<?>, Set<Field>> fieldsMap = new HashMap<Class<?>, Set<Field>> ();

    public interface FieldConsumer extends BiConsumer<SerializableBean, Field> {}

    public interface FieldSetConsumer extends BiConsumer<SerializableBean, Set<Field>> {}

    /**
     * Used to register classes that use the {@link Visitable} annotation to mark properties that will be included in
     * the parameters passed to the function that is executed when the forEachFieldPerBean or forAllFieldsPerBean
     * methods are called.
     */
    public BeanTraversalService<R> register (Class<SerializableBean>... targets) {
        return register (Arrays.asList(targets));
    }

    /**
     * Used to register classes that use the {@link Visitable} annotation to mark properties that will be included in
     * the parameters passed to the function that is executed when the forEachFieldPerBean or forAllFieldsPerBean
     * methods are called.
     */
    public BeanTraversalService<R> register (List<Class<SerializableBean>> targets) {

        targets.forEach(
            target -> {

                // Review : null entry in List ?
                if (target != null && SerializableBean.class.isAssignableFrom(target)) {

                    Set<Field> fields = new HashSet<Field> ();

                    ReflectionUtils.doWithFields(
                        target,
                        new FieldCallback(){

                            @Override
                            public void doWith(final Field field) throws IllegalArgumentException,
                                IllegalAccessException {

                                fields.add(field);
                            }
                        },
                        new FieldFilter() {

                            @Override
                            public boolean matches(final Field field) {
                                return field.isAnnotationPresent(Visitable.class);
                            }
                        }
                    );

                    fieldsMap.put(target, fields);

                } else throw new InvalidConfigurationException ("The target " + target + " must be non-null and "
                    + "of type SerializableBean.");
            }
        );

        return this;
    }

    /**
     * Recursively invokes the function for each serializableBean passing a single field to the function every time.
     */
    public BeanTraversalService<R> forEach (FieldConsumer fieldConsumer, SerializableBean... serializableBeans) {
        return forEach (fieldConsumer, Arrays.asList(serializableBeans));
    }

    /**
     * Recursively invokes the function for each serializableBean passing a single field to the function every time.
     */
    public BeanTraversalService<R> forEach (
        FieldConsumer fieldConsumer,
        List<SerializableBean> serializableBeans
    ) {
        Optional.of(serializableBeans).ifPresent(
            innerItems ->
                innerItems.forEach(
                    serializableBean -> {
                        forEach (fieldConsumer, serializableBean, fieldsMap);
                    }
                )
            );

        return this;
    }

    /**
     * Recursively invokes the function for each serializableBean passing <b>a single field</b> to the function every
     * time.
     */
    private BeanTraversalService<R> forEach (
        FieldConsumer fieldConsumer,
        SerializableBean serializableBean,
        Map<Class<?>, Set<Field>> fieldsMap
    ) {
        Optional.of(serializableBean).ifPresent(
            innerItem -> { 

                Set<Field> fieldSet = fieldsMap.get(innerItem.getClass());

                if (fieldSet != null) {
                    fieldSet.forEach(
                        field -> {
                            forEach (fieldConsumer, serializableBean, field);
                        }
                    );
                }
            }
        );
        return this;
    }

    private BeanTraversalService<R> forEach (
        FieldConsumer fieldConsumer,
        SerializableBean serializableBean,
        Field field
    ) {
        Optional.of(serializableBean).ifPresent(
            innerItem -> {

                if (field.getType().equals (List.class))
                    try {

                        field.setAccessible(true);

                        List<SerializableBean> listField = (List) field.get(serializableBean);

                        field.setAccessible(false);

                        forEach (fieldConsumer, listField);

                    } catch (Exception exception) {
                        throw new GenericReflectionException(
                            "An exception was thrown, see details below (field.name: " + field.getName() + ")",
                            exception
                        );
                    }

                    fieldConsumer.accept(serializableBean, field);
            }
        );
        return this;
    }

    /**
     * Recursively invokes the function for each serializableBean passing <b>all fields</b> to the function every
     * time.
     */
    public BeanTraversalService<R> forEach (
        FieldSetConsumer fieldConsumer,
        SerializableBean... serializableBeans
    ) {
        return forEach (fieldConsumer, Arrays.asList(serializableBeans));
    }

    /**
     * Recursively invokes the function for each serializableBean passing <b>all fields</b> to the function every
     * time.
     */
    public BeanTraversalService<R> forEach (
        FieldSetConsumer fieldSetConsumer,
        List<SerializableBean> serializableBeans
    ) {
        Optional.of(serializableBeans).ifPresent(
            beans ->
                beans.forEach(
                    serializableBean -> {
                        forEach (fieldSetConsumer, serializableBean, fieldsMap);
                    }
                )
            );

        return this;
    }

    private BeanTraversalService<R> forEach (
        FieldSetConsumer fieldSetConsumer,
        SerializableBean serializableBean,
        Map<Class<?>, Set<Field>> fieldsMap
    ) {
        Optional.of(serializableBean).ifPresent(
            innerItem -> { 

                Set<Field> fieldSet = fieldsMap.get(innerItem.getClass());

                fieldSetConsumer.accept(serializableBean, fieldSet);

                fieldSet.forEach(
                    field -> {
                        if (field.getType().equals(List.class)) {

                            field.setAccessible(true);

                            List<SerializableBean> serializableBeans;

                            try {
                                serializableBeans = (List) field.get(serializableBean);
                            } catch (Exception exception) {
                                throw new GenericReflectionException(
                                    "Unable to get the field with name: " + field.getName(),
                                    exception
                                );
                            }

                            forEach (fieldSetConsumer, serializableBeans);

                            field.setAccessible(false);
                        }
                    }
                );
            }
        );
        return this;
    }
}
