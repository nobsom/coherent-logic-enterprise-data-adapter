package com.coherentlogic.coherent.data.adapter.core.plugins;

import com.coherentlogic.coherent.data.adapter.core.exceptions.PluginNotFoundException;

public class DefaultPluginContainer implements PluginContainerSpecification {

    @Override
    public <T> T lookup(String name) {
        throw new PluginNotFoundException ("No plugins are available for this plugin container.");
    }

    @Override
    public <T> T lookup(Class<?> type) {

        if (type == null)
            throw new NullPointerException("The type is null.");

        return lookup (type.toString());
    }
}
