package com.coherentlogic.coherent.data.adapter.core.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * Used to convert an instance of {@link javax.script.ScriptException} into an instance of
 * {@link org.springframework.core.NestedRuntimeException}.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class ScriptRuntimeException extends NestedRuntimeException {

	private static final long serialVersionUID = -3222268618443163738L;

	public ScriptRuntimeException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public ScriptRuntimeException(String msg) {
		super(msg);
	}
}
