package com.coherentlogic.coherent.data.adapter.core.cache;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * Use this class with cache providers that implement the java.util.Map interface (JBoss Infinispan and Oracle Coherence
 * are two examples).
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 * @param <K> The key type.
 * @param <V> The value type.
 *
 * @deprecated This class is no longer being used and will be deleted.
 */
public class MapCompliantCacheServiceProvider<K> implements CacheServiceProviderSpecification<K> {

    private static final Logger log = LoggerFactory.getLogger(MapCompliantCacheServiceProvider.class);

    private final Map<K, SerializableBean> cache;

    public MapCompliantCacheServiceProvider(Map<K, SerializableBean> cache) {
        this.cache = cache;
    }

    @Override
    public <V extends SerializableBean> V get(K key) {

        log.debug("get: method begins; key: " + key);

        V value = (V) cache.get(key);

        log.debug("get: method ends; value: " + value);

        return value;
    }

    @Override
    public <V extends SerializableBean> void put(K key, V value) {

        log.debug("put: method begins; key: " + key + ", value: " + value);

        cache.put(key, value);

        log.debug("put: method ends.");
    }

    @Override
    public String toString() {
        return "MapCompliantCacheServiceProvider [cache=" + cache + "]";
    }
}
