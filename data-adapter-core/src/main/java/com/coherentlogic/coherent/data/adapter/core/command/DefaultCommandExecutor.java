package com.coherentlogic.coherent.data.adapter.core.command;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.coherentlogic.coherent.data.adapter.core.builders.AbstractQueryBuilder;

/**
 * An implementation of the CommandExecutorSpecification which executes the command immediately in
 * <i>this</i> thread.
 *
 * Note that the future returned cannot be cancelled (invoking cancel or isCancelled will return false)
 * and invoking the get method will return the result immediately.
 *
 * Note that the function is invoked by <i>this</i> thread.
 *
 * @param <X> The key type of the AbstractQueryBuilder.
 */
public class DefaultCommandExecutor<K> implements CommandExecutorSpecification<K> {

	@Override
	public <R> Future<R> invoke(Map<String, Object> context, Function<Map<String, Object>, R> function) {

        return new Future<R> () {

            R result = function.apply(context);

            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                return false;
            }

            @Override
            public boolean isCancelled() {
                return false;
            }

            @Override
            public boolean isDone() {
                return true;
            }

            @Override
            public R get() throws InterruptedException, ExecutionException {
                return result;
            }

            @Override
            public R get(long timeout, TimeUnit unit)
                throws InterruptedException, ExecutionException, TimeoutException {

                return result;
            }
        };
      }

//    @Override
//    public <X extends Map<String, Object>, Y> Future<Y> invoke(Function<X, Y> function) {
//
//        return new Future<Y> () {
//
//          Y result = function.apply(X);
//
//          @Override
//          public boolean cancel(boolean mayInterruptIfRunning) {
//              return false;
//          }
//
//          @Override
//          public boolean isCancelled() {
//              return false;
//          }
//
//          @Override
//          public boolean isDone() {
//              return true;
//          }
//
//          @Override
//          public Y get() throws InterruptedException, ExecutionException {
//              return result;
//          }
//
//          @Override
//          public Y get(long timeout, TimeUnit unit)
//              throws InterruptedException, ExecutionException, TimeoutException {
//
//              return result;
//          }
//      };
//    }

//	@Override
//	public <X extends AbstractQueryBuilder<K>, R> Future<R> invoke(
//		X queryBuilder, BiFunction<X, Map<String, ?>, R> function) {
//
//		final Map<String, ?> context = (Map<String, ?>) new HashMap ();
//
//		return new Future<R> () {
//
//          R result = function.apply(queryBuilder, context);
//
//          @Override
//          public boolean cancel(boolean mayInterruptIfRunning) {
//              return false;
//          }
//
//          @Override
//          public boolean isCancelled() {
//              return false;
//          }
//
//          @Override
//          public boolean isDone() {
//              return true;
//          }
//
//          @Override
//          public R get() throws InterruptedException, ExecutionException {
//              return result;
//          }
//
//          @Override
//          public R get(long timeout, TimeUnit unit)
//              throws InterruptedException, ExecutionException, TimeoutException {
//
//              return result;
//          }
//      };
//    }
}
