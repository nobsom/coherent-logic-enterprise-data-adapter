package com.coherentlogic.coherent.data.adapter.core.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * An exception that is thrown when the configuration is invalid.
 */
public class InvalidConfigurationException extends NestedRuntimeException {

    private static final long serialVersionUID = -1701816115313452516L;

    public InvalidConfigurationException(String msg) {
        super(msg);
    }

    public InvalidConfigurationException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
