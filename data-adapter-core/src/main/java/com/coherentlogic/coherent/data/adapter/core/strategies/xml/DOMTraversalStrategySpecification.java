package com.coherentlogic.coherent.data.adapter.core.strategies.xml;

import java.util.function.Function;

import org.w3c.dom.Node;

/**
 * Used to define custom, breadth, and depth-first traversal strategies.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@FunctionalInterface
public interface DOMTraversalStrategySpecification {

    /**
     * Method traverses the DOM document nodes and executes the function where appropriate.
     * 
     * @param node The DOM {@link Node}.
     *
     * @param function A function that takes a {@link Node} and returns a result of type R.
     *
     * @return A result of type R.
     */
    <R> R execute (Node node, Function<Node, R> function);
}
