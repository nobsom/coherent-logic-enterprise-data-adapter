package com.coherentlogic.coherent.data.adapter.core.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * A cache service provider implementation which is used when no caching is configured.
 *
 * @param <K> The key type.
 * @param <V> The value type.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class NullCache<K> implements CacheServiceProviderSpecification<K> {

    private static final Logger log = LoggerFactory.getLogger(NullCache.class);

    @Override
    public <V extends SerializableBean> V get(K key) {
        return null;
    }

    @Override
    public <V extends SerializableBean> void put(K key, V value) {
        log.debug (this + " is in use so the put operation for key: " + key + " and value: " + value +
            " will be ignored.");
    }
}
