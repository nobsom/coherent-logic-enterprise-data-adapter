package com.coherentlogic.coherent.data.adapter.core.builders;

import com.coherentlogic.coherent.data.model.core.command.CommandSpecification;

/**
 * Specification for query builders that accept commands to be executed on data. Implementations of this interface
 * should add the {@link CommandSpecification} to the command executor, which will execute all commands just before
 * returning the data to the caller.
 */
public interface WithCommandSpecification<T extends AbstractQueryBuilder<?>> {

    T withCommand (CommandSpecification command);
}
