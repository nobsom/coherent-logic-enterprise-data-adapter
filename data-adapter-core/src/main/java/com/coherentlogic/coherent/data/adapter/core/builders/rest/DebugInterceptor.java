package com.coherentlogic.coherent.data.adapter.core.builders.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class DebugInterceptor implements ClientHttpRequestInterceptor {

    final static Logger log = LoggerFactory.getLogger(DebugInterceptor.class);

    @Override
    public ClientHttpResponse intercept(
        HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {

        logRequest(request, body);

        ClientHttpResponse response = execution.execute(request, body);

        logResponse(response);

        return response;
    }

    private void logRequest(HttpRequest request, byte[] body) throws IOException {

        log.info("====================================== Request Begins ==================================");
        log.info("uri        : ", request.getURI());
        log.info("method     : ", request.getMethod());
        log.info("headers    : ", request.getHeaders() );
        log.info("requestBody: ", new String(body, "UTF-8"));
        log.info("===================================== Request Ends =====================================");
    }

    private void logResponse(ClientHttpResponse response) throws IOException {

        StringBuilder inputStringBuilder = new StringBuilder();

        BufferedReader bufferedReader =
            new BufferedReader(new InputStreamReader(response.getBody(), "UTF-8"));

        bufferedReader.lines().forEach(
            line -> {
                inputStringBuilder.append(line).append('\n');
            }
        );

        log.info("======================================= Response Begins ================================");
        log.info("statusCode  : " + response.getStatusCode());
        log.info("statusText  : " + response.getStatusText());
        log.info("headers     : " + response.getHeaders());
        log.info("responseBody: " + inputStringBuilder);
        log.info("======================================= Response Ends ==================================");
    }
}
