package com.coherentlogic.coherent.data.adapter.core.builders.rest;

import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.command.CommandExecutorSpecification;

public class ExampleRESTQueryBuilder extends AbstractRESTQueryBuilder<String> {

    public static final String FOO = "foo", BAR = "bar", BAZ = "baz";

    protected ExampleRESTQueryBuilder(RestTemplate restTemplate, String uri) {
        super (restTemplate, uri);
    }

    public ExampleRESTQueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String> cache,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, uri, cache, commandExecutor);
    }

    protected ExampleRESTQueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uri, cache);
    }

    /**
     * Extends the path to include 'foo' -- for example:
     *
     * http://www.foobar.zzz/foo/
     */
    public ExampleRESTQueryBuilder foo () {

        extendPathWith(FOO);

        return this;
    }

    /**
     * Setter method for the bar parameter.
     */
    public ExampleRESTQueryBuilder setBar (String bar) {

        addParameter(BAR, bar);

        return this;
    }

    @Override
    public String getCacheKey() {
        return getEscapedURI();
    }

    @Override
    protected <T> T doExecute(Class<T> type) {
        return null;
    }
}
