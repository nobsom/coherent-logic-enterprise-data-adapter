package com.coherentlogic.coherent.data.adapter.core.builders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.config.ConstructorArgumentValues.ValueHolder;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.ApplicationContext;

import com.coherentlogic.coherent.data.adapter.core.exceptions.InvalidConfigurationException;
import com.coherentlogic.coherent.data.adapter.core.plugins.DefaultPlugin;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * Unit test for the {@link AbstractQueryBuilder} class.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class AbstractQueryBuilderTest {

    final static class ExampleQueryBuilder extends AbstractQueryBuilder<String> {};

    private ExampleQueryBuilder exampleQueryBuilder = null;

    @BeforeEach
    public void setUp() {

        System.clearProperty(FOOOOOOO);

        exampleQueryBuilder = new ExampleQueryBuilder ();
    }

    @AfterEach
    public void tearDown() {

        System.clearProperty(FOOOOOOO);

        this.exampleQueryBuilder = null;
    }

    static class HelloWorldBean {

        private final String value;

        public HelloWorldBean (String value) {
            this.value = value;
        }

        public String getValue () {
            return value;
        }
    }

    @Test
    public void testRegisterBeanAndInvokeCommandSuccessfully () throws InterruptedException, ExecutionException {

        GenericBeanDefinition beanDefinition = new GenericBeanDefinition ();

        beanDefinition.setBeanClass(HelloWorldBean.class);

        ConstructorArgumentValues constructorArgumentValues = new ConstructorArgumentValues ();

        String RESULT = "Hello World!";

        constructorArgumentValues.addGenericArgumentValue(new ValueHolder (RESULT));

        beanDefinition.setConstructorArgumentValues(constructorArgumentValues);

        exampleQueryBuilder.register(new DefaultPlugin ("helloWorld", beanDefinition));

        Future<String> result =
            exampleQueryBuilder.invoke(
                (Map<String, Object> context) -> {

                    ApplicationContext applicationContext = (ApplicationContext) context.get("applicationContext");

                    HelloWorldBean helloWorldBean = applicationContext.getBean(HelloWorldBean.class);

                    return helloWorldBean.getValue();
                }
            );

        assertEquals (RESULT, result.get());
    }

    @Test
    public void testInvokeSuccessfully () throws InterruptedException, ExecutionException {

        Future<Integer> result =
            exampleQueryBuilder.invoke((Map<String, Object> context) -> { System.out.println ("context: " + context); return Integer.MAX_VALUE; });

        assertEquals (new Integer (Integer.MAX_VALUE), result.get());
    }

    @Test
    public void testInvokeAndThrowAnException () throws InterruptedException, ExecutionException {

        assertThrows (
            RuntimeException.class,
            () -> {
                Future<Integer> result =
                    exampleQueryBuilder.invoke(
                        (Map<String, Object> context) -> { throw new RuntimeException("Expected."); }
                    );

                assertEquals (new Integer (Integer.MAX_VALUE), result.get());
            }
        );
    }

    @Test
    public void testForEachFieldPerBeanBiConsumerOfSerializableBeanOfQFieldSerializableBeanOfQArray() {

        ExampleD exampleD = new ExampleD();

        AtomicInteger ctr = new AtomicInteger();

        exampleQueryBuilder.accept(
            serializableBean -> {
                ctr.incrementAndGet();
            },
            exampleD
        );

        assertEquals(3, ctr.get());
    }

    @Test
    public void testForAllFieldsPerBeanBiConsumerOfSerializableBeanOfQSetOfFieldSerializableBeanOfQArray() {

        ExampleB exampleB = new ExampleB();

        AtomicInteger ctr = new AtomicInteger();

        exampleQueryBuilder.accept(
            serializableBean -> {
                ctr.incrementAndGet();
            },
            exampleB
        );

        assertEquals(3, ctr.get());
    }

    @Test
    public void testForAllForExampleD() {

        ExampleD exampleD = new ExampleD();

        AtomicInteger ctr = new AtomicInteger();

        exampleQueryBuilder.accept(
            serializableBean -> {
                ctr.incrementAndGet();
            },
            exampleD
        );

        assertEquals(3, ctr.get());
    }

    private static final String FOOOOOOO = "FOOOOOOO", BAR = "bar";

    @Test
    public void testGetNullProperty () {
        assertThrows (
            InvalidConfigurationException.class,
            () -> {
                exampleQueryBuilder.getProperty("FOOOOOOO"); // Assuming FOOOOOOO is not being used.
            }
        );
    }

    @Test
    public void testGetProperty () {

        System.setProperty(FOOOOOOO, BAR);

        assertEquals (BAR, exampleQueryBuilder.getProperty(FOOOOOOO)); // Assuming FOOOOOOO is not being used.
    }

    @Test
    public void testGetPropertyWithNonNullVMArg () {

        System.setProperty(FOOOOOOO, BAR);

        assertEquals (BAR, exampleQueryBuilder.getProperty(FOOOOOOO)); // Assuming FOOOOOOO is not being used.
    }

    @Test
    public void testGetPropertyWithNullVMArgAndNonNullEnvProperty () {

        Map<String, String> env = new HashMap<String, String> ();

        env.put(FOOOOOOO, BAR);

        assertEquals (BAR, exampleQueryBuilder.getProperty(env, FOOOOOOO)); // Assuming FOOOOOOO is not being used.

        env.remove(FOOOOOOO);
    }
}

class ExampleA extends SerializableBean {

    private static final long serialVersionUID = 266217980008494678L;

    @Visitable
    private List<ExampleB> exampleBList = new ArrayList<ExampleB>();

    @Visitable
    private final ExampleD exampleD1 = new ExampleD();
    @Visitable
    private final ExampleD exampleD2 = new ExampleD();

    // Should not be scanned.
    private final BigDecimal someOtherBigDecimalValue = new BigDecimal("456.789");

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        super.accept(visitors);

        if (Optional.of(visitors).isPresent()) {
            visitors.forEach(
                (Consumer<SerializableBean> visitor) -> {
                exampleBList.forEach(
                    entry -> {
                        visitor.accept(entry);
                    }
                );
            });
        }
    }
}

class ExampleB extends SerializableBean {

    private static final long serialVersionUID = -5283781765327466207L;

    @Visitable
    private List<ExampleC> exampleCList = new ArrayList<ExampleC>();

    @Visitable
    private final BigDecimal someBigDecimalValue = new BigDecimal("123.456");

    // Should not be scanned.
    private final BigDecimal someOtherBigDecimalValue = new BigDecimal("456.789");

    // Arrays at this time will not be inspected so we'll see this field appear
    // once but the contents will not be
    // inspected.
    @Visitable
    private ExampleC[] exampleCArray = new ExampleC[] { new ExampleC(), new ExampleC() };

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        super.accept(visitors);

        if (Optional.of(visitors).isPresent()) {
            visitors.forEach(visitor -> {
                Arrays.asList(exampleCArray).forEach(
                    entry -> {
                        visitor.accept(entry);
                    }
                );
            });
        }
    }
}

class ExampleC extends SerializableBean {

    private static final long serialVersionUID = 4565190252217346381L;

    @Visitable
    private final BigDecimal someBigDecimalValue = new BigDecimal("123.456");

    // Should not be scanned.
    private final BigDecimal someOtherBigDecimalValue = new BigDecimal("456.789");
}

class ExampleD extends ExampleB {
    private static final long serialVersionUID = -8136668897423713832L;
}
