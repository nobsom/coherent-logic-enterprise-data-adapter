package com.coherentlogic.coherent.data.adapter.aop.domain;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * Test domain class.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Foo extends SerializableBean {

    private static final long serialVersionUID = -447908556633676785L;

    static final String BAR = "bar";

    private String bar, baz;

    public String getBar() {
        return bar;
    }

    public void setBar(@Changeable (BAR) String bar) {
        this.bar = bar;
    }

    public String getBaz() {
        return baz;
    }

    public void setBaz(String baz) { // NOTE: Lack of @Changeable ("baz") is deliberate.
        this.baz = baz;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((bar == null) ? 0 : bar.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Foo other = (Foo) obj;
        if (bar == null) {
            if (other.bar != null)
                return false;
        } else if (!bar.equals(other.bar))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Foo [bar=" + bar + ", baz=" + baz + "]";
    }
}
