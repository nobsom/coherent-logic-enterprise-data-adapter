package com.coherentlogic.coherent.data.adapter.aop.aspects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;

import com.coherentlogic.coherent.data.adapter.aop.domain.Foo;

/**
 * Unit test for the {@link DefaultPropertyChangeGeneratorAspect} class.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class DefaultPropertyChangeGeneratorAspectTest {

    private Foo foo;

    @BeforeEach
    public void before () {
        this.foo = new Foo ();
    }

    @AfterEach
    public void after () {
        this.foo = null;
    }

    /**
     * Note that if the aspect has not been weaved into the class then this test will fail when running this
     * from within Eclipse.
     * 
     * Also note the following strange behavior: Eclipse was showing a "project not up to date" problem and
     * this test was failing, both here and from the command line via Maven. Oddly, when I updated the
     * project and the AspectJ-related issue went away this test then passed.
     */
    @Test
    public void testSetBar () throws Throwable {

        final AtomicBoolean flag = new AtomicBoolean(false);

        foo.addPropertyChangeListener(event -> { flag.set(true); });

        foo.setBar("bar");

        assertTrue (flag.get());
    }

    /**
     * Note that if the aspect has not been weaved into the class then this test will not be valid when
     * running this from within Eclipse.
     */
    @Test
    public void testSetBaz () throws Throwable {

        final AtomicBoolean flag = new AtomicBoolean(false);

        foo.addPropertyChangeListener(event -> { flag.set(true); });

        foo.setBaz("baz");

        assertFalse (flag.get());
    }
}
