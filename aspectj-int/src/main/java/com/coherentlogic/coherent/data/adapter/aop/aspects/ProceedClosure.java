package com.coherentlogic.coherent.data.adapter.aop.aspects;

/**
 * Method that wraps the call to proceed.
 *
 * @author thospfuller
 *
 * @param <T> The type of result returned from the call to {@link #proceed()}.
 */
@FunctionalInterface
public interface ProceedClosure<T> {

    T proceed () throws Throwable;
}
