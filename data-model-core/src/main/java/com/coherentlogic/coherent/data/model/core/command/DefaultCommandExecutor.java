package com.coherentlogic.coherent.data.model.core.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * Implementation of the {@link CommandExecutorSpecification} for commands that are executed against instanced of
 * {@link SerializableBean}.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class DefaultCommandExecutor {//implements CommandExecutorSpecification {

//    private static final Logger log = LoggerFactory.getLogger(DefaultCommandExecutor.class);
//
//    private final Collection<CommandSpecification> commands;
//
//    public DefaultCommandExecutor () {
//        this (new ArrayList<CommandSpecification> ());
//    }
//
//    /**
//     * Constructor which takes an initial set of commands to be executed.
//     *
//     * @param commands An initial set of commands to be executed.
//     */
//    public DefaultCommandExecutor (Collection<CommandSpecification> commands) {
//        this.commands = commands;
//    }
//
//    @SafeVarargs
//    public final <S extends SerializableBean> void execute(S serializableBean, CommandSpecification... commands) {
//        execute (serializableBean, Arrays.asList(commands));
//    }
//
//    @Override
//    public <S extends SerializableBean> void addCommand(CommandSpecification... commands) {
//        addCommand (Arrays.asList(commands));
//    }
//
//    @Override
//    public <S extends SerializableBean> void addCommand(Collection<CommandSpecification> commands) {
//        this.commands.addAll(commands);
//    }
//
//    @Override
//    public <S extends SerializableBean> void execute(S serializableBean) {
//        execute (serializableBean, commands);
//    }
//
//    public final <S extends SerializableBean> void execute(S serializableBean, Collection<CommandSpecification> commands) {
//
//        log.debug("execute: method begins; serializableBean: " + serializableBean + ", commands: " + commands);
//
//        for (CommandSpecification command : commands) {
//
//            log.debug("execute: pre-execute of command: " + command);
//
//            command.execute (serializableBean);
//
//            log.debug("execute: post-execute of command: " + command);
//        }
//
//        log.debug("execute: method ends.");
//    }
}
