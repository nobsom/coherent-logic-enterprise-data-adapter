package com.coherentlogic.coherent.data.model.core.strategies;

import java.io.Serializable;

/**
 * Specification for the strategy pattern that will be used to customize behavior on an object of type T.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 * @param <T> The target type.
 */
public interface StrategySpecification<T> extends Serializable {

    /**
     * Method performs the operation on the target.
     *
     * @param target The instance of type T that will have the operation applied.
     */
    void execute (T target);
}
