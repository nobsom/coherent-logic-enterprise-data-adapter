package com.coherentlogic.coherent.data.model.core.domain;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.coherentlogic.coherent.data.model.core.command.CommandExecutorSpecification;
import com.coherentlogic.coherent.data.model.core.exceptions.CloneFailedException;
import com.coherentlogic.coherent.data.model.core.listeners.AggregatePropertyChangeEvent;
import com.coherentlogic.coherent.data.model.core.listeners.AggregatePropertyChangeListener;
import com.coherentlogic.coherent.data.model.core.strategies.DefaultTraversalStrategy;
import com.coherentlogic.coherent.data.model.core.strategies.TraversalStrategySpecification;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * A class which all domain classes inherit from.
 *
 * @todo Consider removing the guard around calls to fire methods as the various ChangeSupport references should not
 *  really ever be null.
 *
 * @todo Can we utilize the ExceptionListener here?
 *
 * @see <a href="http://docs.oracle.com/javase/7/docs/api/java/beans/
 *  PropertyChangeSupport.html">PropertyChangeSupport</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
//@Entity()
//@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
//@Table(name=SerializableBean.SERIALIZABLE_BEAN)
@MappedSuperclass
public class SerializableBean
    implements Serializable, Cloneable, AcceptorSpecification, CommandExecutorSpecification {

    private static final long serialVersionUID = 3557664417023869095L;

    public static final String
        SERIALIZABLE_BEAN = "serializable_bean",
        PRIMARY_KEY = "primaryKey",
        CREATED_TIME_MILLIS = "createdTimeMillis",
        CREATED_DATE = "createdDate",
        UPDATED_DATE = "updatedDate",
        VERSION = "version";

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long primaryKey = null;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdDate", nullable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updatedDate", nullable = false)
    private Date updatedDate;

    @Version
    @Column(name = "optlock", columnDefinition = "integer DEFAULT 0", nullable = false)
    @XStreamOmitField()
    private long version = 0L;

    /**
     * @see {@link java.beans.VetoableChangeSupport}
     */
    @Transient
    @XStreamOmitField
    private transient final VetoableChangeSupport vetoableChangeSupport;

    /**
     * Note that this property must be set to a <b>non-null value</b> otherwise
     * attempts to register an instance of {@link PropertyChangeListener} with
     * an instance of this class will fail, as well as any attempt to invoke the
     * {@link java.beans.PropertyChangeSupport#firePropertyChange(
     *  java.beans.PropertyChangeEvent)} method (and/or possibly other methods).
     *
     * Note that the {@link #propertyChangeSupport} property is not final. Since
     * we're using XStream and this class implements {@link
     * java.io.Serializable} then we need to either supply a readObject method
     * or the {@link #propertyChangeSupport} property will be null when XStream
     * unmarshalls the XML. We don't want to set this property to final,
     * however, as the user may want to use a different default value than the
     * one we supply.
     *
     * @see {@link java.beans.PropertyChangeSupport}
     */
    @Transient
    @XStreamOmitField
    public transient final PropertyChangeSupport propertyChangeSupport;

    @Transient
    @XStreamOmitField
    public transient final List<AggregatePropertyChangeListener<SerializableBean>> aggregatePropertyChangeListeners;

    static PropertyChangeSupport createDefaultPropertyChangeSupport (SerializableBean serializableBean) {
        return new PropertyChangeSupport(serializableBean);
    }

    public SerializableBean() {
        propertyChangeSupport = createDefaultPropertyChangeSupport (this);
        vetoableChangeSupport = new VetoableChangeSupport (this);
        this.aggregatePropertyChangeListeners = new ArrayList<AggregatePropertyChangeListener<SerializableBean>> ();
    }

    public SerializableBean(
        PropertyChangeSupport propertyChangeSupport,
        VetoableChangeSupport vetoableChangeSupport,
        List<AggregatePropertyChangeListener<SerializableBean>> aggregatePropertyChangeListeners
    ) {
        super();
        this.propertyChangeSupport = propertyChangeSupport;
        this.vetoableChangeSupport = vetoableChangeSupport;
        this.aggregatePropertyChangeListeners = aggregatePropertyChangeListeners;
    }

    @PrePersist
    protected void onCreate() {
        createdDate = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updatedDate = new Date();
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {

        Date oldValue = this.createdDate;

        this.createdDate = createdDate;

        firePropertyChange(CREATED_DATE, oldValue, createdDate);
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {

        Date oldValue = this.updatedDate;

        this.updatedDate = updatedDate;

        firePropertyChange(UPDATED_DATE, oldValue, updatedDate);
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {

        long oldValue = this.version;

        this.version = version;

        firePropertyChange(VERSION, oldValue, version);
    }

    @Id
    @GeneratedValue(strategy=GenerationType.TABLE)
    public Long getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(Long primaryKey) {

        Long oldValue = this.primaryKey;

        this.primaryKey = primaryKey;

        firePropertyChange(PRIMARY_KEY, oldValue, primaryKey);
    }

    /**
     * Method returns a copy of the date parameter or null if the reference to
     * the date is null.
     *
     * @param date The date to be cloned.
     */
    protected static Date clone (Date date) {

        Date newDate = null;

        if (date != null)
            newDate = (Date) date.clone();

        return newDate;
    }

    public <X> X clone (Class<X> type) {

        Object object = clone ();

        X result = type.cast(object);

        return result;
    }

    @Override
    public Object clone () {
        try {
            return BeanUtils.cloneBean(this);
        } catch (IllegalAccessException illegalAccessException) {
            throw new CloneFailedException("Illegal access exception thrown", illegalAccessException);
        } catch (InstantiationException instantiationException) {
            throw new CloneFailedException("Failed to instantiate the class.", instantiationException);
        } catch (InvocationTargetException invocationTargetException) {
            throw new CloneFailedException("Invocation target exception thrown.", invocationTargetException);
        } catch (NoSuchMethodException noSuchMethodException) {
            throw new CloneFailedException("No such method exception thrown.", noSuchMethodException);
        }
    }

    /**
     * Getter method for the {@link VetoableChangeSupport} instance.
     */
    @Transient
    public VetoableChangeSupport getVetoableChangeSupport() {
        // Why is this marked as transient?
        // http://stackoverflow.com/questions/3022958/
        // java-hibernate-annotations-how-to-add-methods-to-pojo-object
        return vetoableChangeSupport;
    }

    /**
     * Method adds the specified listener to the {@link #vetoableChangeSupport}
     * instance.
     */
    public void addVetoableChangeListener(VetoableChangeListener listener) {
        vetoableChangeSupport.addVetoableChangeListener(listener);
    }

    /**
     * Method removes the specified listener to the {@link
     * #vetoableChangeSupport} instance.
     */
    public void removeVetoableChangeListener(VetoableChangeListener listener) {
        vetoableChangeSupport.removeVetoableChangeListener(listener);
    }

    /**
     * Getter method for the {@link #propertyChangeSupport} property.
     */
    @Transient
    public PropertyChangeSupport getPropertyChangeSupport() {
        return propertyChangeSupport;
    }

    /**
     * Method adds the specified listener to the {@link #propertyChangeSupport} instance.
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Method removes the specified listener from the {@link #propertyChangeSupport} instance.
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    /**
     * Method determines if the firePropertyChange method should be called.
     *
     * The documentation for the PropertyChangeSupport class states that "[n]o event is fired if old and new values are
     * equal and non-null." and this is what this method is checking for.
     *
     * @see https://docs.oracle.com/javase/7/docs/api/java/beans/PropertyChangeSupport.html
     * @see https://docs.oracle.com/javase/tutorial/javabeans/writing/properties.html
     */
    static boolean propertiesDiffer (Object oldValue, Object newValue) {

        boolean result = false;

        if ((oldValue != null && !oldValue.equals(newValue)) || (oldValue == null && newValue != null))
            result = true;

        return result;
    }

    protected boolean propertiesDiffer (int oldValue, int newValue) {

        boolean result = false;

        if (oldValue != newValue)
            result = true;

        return result;
    }

    protected boolean propertiesDiffer (boolean oldValue, boolean newValue) {

        boolean result = false;

        if (oldValue != newValue)
            result = true;

        return result;
    }

    /**
     * Method delegates to the
     * {@link java.beans.PropertyChangeSupport#firePropertyChange(
     * String, Object, Object)} method.
     */
    protected SerializableBean firePropertyChange (
        String propertyName,
        Object oldValue,
        Object newValue
    ) {
        return firePropertyChange (new PropertyChangeEvent (this, propertyName, oldValue, newValue));
    }

    /**
     * Method delegates to the
     * {@link java.beans.PropertyChangeSupport#firePropertyChange(
     * String, boolean, boolean)} method.
     */
    protected SerializableBean firePropertyChange (
        String propertyName,
        boolean oldValue,
        boolean newValue
    ) {
        return firePropertyChange (new PropertyChangeEvent (this, propertyName, oldValue, newValue));
    }

    /**
     * Method delegates to the {@link java.beans.PropertyChangeSupport#firePropertyChange(String, int, int)} method.
     */
    protected SerializableBean firePropertyChange (
        String propertyName,
        int oldValue,
        int newValue
    ) {
        return firePropertyChange (new PropertyChangeEvent (this, propertyName, oldValue, newValue));
    }

    /**
     * Method delegates to the
     * {@link java.beans.PropertyChangeSupport#firePropertyChange(
     * PropertyChangeEvent)}
     * method.
     */
    protected SerializableBean firePropertyChange (PropertyChangeEvent propertyChangeEvent) {

        if (propertiesDiffer(propertyChangeEvent.getOldValue(), propertyChangeEvent.getNewValue()))
            /* If you see an NPE here when using XStream then you've forgotten to assign the
             * CustomMarshallingStrategy to the XStreamMarshaller, as we have below: 
             *
             * <bean id="xstreamMarshaller" class="com.coherentlogic.fred.client.core.oxm.xstream.XStreamMarshaller">
             *     <property name="marshallingStrategy">
             *         <bean class="com.coherentlogic.coherent.data.adapter.core.xstream.CustomMarshallingStrategy"/>
             *     </property>
             *     ...
             * </bean>
             *
             * If you see the same exception after adding the marshallingStrategy bean then Spring may be loading more
             * than one application context file -- check the beginning of the log file for something like below and
             * note that, if you see this, you likely have the xstreamMarshaller misconfigured in more than one place:
             *
             * 2017-11-17 13:08:40,016 INFO  ClassPathXmlApplicationContext - Refreshing org.springframework.context.support.ClassPathXmlApplicationContext@7ca48474: startup date [Fri Nov 17 13:08:40 EST 2017]; root of context hierarchy
             * 2017-11-17 13:08:40,090 INFO  XmlBeanDefinitionReader - Loading XML bean definitions from class path resource [spring/db/application-context.xml]
             * 2017-11-17 13:08:40,606 INFO  XmlBeanDefinitionReader - Loading XML bean definitions from class path resource [spring/db/h2-jpa-beans.xml]
             * 2017-11-17 13:08:42,113 INFO  XmlBeanDefinitionReader - Loading XML bean definitions from URL [file:/C:/Users/.../fred-client-example/target/classes/spring/spark/api-key-beans.xml]
             * 2017-11-17 13:08:42,136 INFO  XmlBeanDefinitionReader - Loading XML bean definitions from URL [file:/C:/Users/.../git/fred-client-example/target/classes/spring/spark/application-context.xml]
             * 2017-11-17 13:08:42,416 INFO  DefaultListableBeanFactory - Overriding bean definition for bean 'fredRestTemplate' with a different definition: replacing [Generic bean: class [org.springframework.web.client.RestTemplate]; scope=; abstract=false; lazyInit=false; autowireMode=0; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null; defined in class path resource [spring/db/application-context.xml]] with [Generic bean: class [org.springframework.web.client.RestTemplate]; scope=; abstract=false; lazyInit=false; autowireMode=0; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null; defined in URL [file:/C:/Users/.../fred-client-example/target/classes/spring/spark/application-context.xml]]
             * 2017-11-17 13:08:42,707 INFO  DefaultListableBeanFactory - Overriding bean definition for bean 'org.springframework.transaction.config.internalTransactionAdvisor' with a different definition: replacing [Root bean: class [org.springframework.transaction.interceptor.BeanFactoryTransactionAttributeSourceAdvisor]; scope=; abstract=false; lazyInit=false; autowireMode=0; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=null; factoryMethodName=null; initMethodName=null; destroyMethodName=null] with [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=org.springframework.transaction.annotation.ProxyTransactionManagementConfiguration; factoryMethodName=transactionAdvisor; initMethodName=null; destroyMethodName=(inferred); defined in class path resource [org/springframework/transaction/annotation/ProxyTransactionManagementConfiguration.class]]
             * 2017-11-17 13:08:42,708 INFO  ConfigurationClassBeanDefinitionReader - Skipping bean definition for [BeanMethod:name=transactionalEventListenerFactory,declaringClass=org.springframework.transaction.annotation.AbstractTransactionManagementConfiguration]: a definition for bean 'org.springframework.transaction.config.internalTransactionalEventListenerFactory' already exists. This top-level bean definition is considered as an override.
             */
            propertyChangeSupport.firePropertyChange(propertyChangeEvent);

        return this;
    }

    /**
     * Method delegates to the
     * {@link java.beans.PropertyChangeSupport#fireIndexedPropertyChange(String,
     * int, Object, Object)} method.
     */
    protected SerializableBean fireIndexedPropertyChange (
        String propertyName,
        int index,
        Object oldValue,
        Object newValue
    ) {
        propertyChangeSupport.fireIndexedPropertyChange(propertyName, index, oldValue, newValue);

        return this;
    }

    /**
     * Method delegates to the
     * {@link java.beans.PropertyChangeSupport#fireIndexedPropertyChange(String,
     * int, int, int)} method.
     */
    protected SerializableBean fireIndexedPropertyChange (
        String propertyName,
        int index,
        int oldValue,
        int newValue
    ) {
        propertyChangeSupport.fireIndexedPropertyChange(propertyName, index, oldValue, newValue);

        return this;
    }

    /**
     * Method delegates to the
     * {@link java.beans.PropertyChangeSupport#fireIndexedPropertyChange(String,
     * int, boolean, boolean)} method.
     */
    protected SerializableBean fireIndexedPropertyChange (
        String propertyName,
        int index,
        boolean oldValue,
        boolean newValue
    ) {
        propertyChangeSupport.fireIndexedPropertyChange(propertyName, index, oldValue, newValue);

        return this;
    }

    /**
     * Delegates to {@link VetoableChangeSupport#fireVetoableChange(String, Object, Object)}.
     *
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/beans/VetoableChangeSupport.html">VetoableChangeSupport</a>
     */
    protected SerializableBean fireVetoableChange (String propertyName, Object oldValue, Object newValue)
        throws PropertyVetoException {

        vetoableChangeSupport.fireVetoableChange(propertyName, oldValue, newValue);

        return this;
    }

    /**
     * Delegates to {@link VetoableChangeSupport#fireVetoableChange(String, boolean, boolean)}.
     *
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/beans/VetoableChangeSupport.html">VetoableChangeSupport</a>
     */
    protected SerializableBean fireVetoableChange (String propertyName, boolean oldValue, boolean newValue)
        throws PropertyVetoException {

        vetoableChangeSupport.fireVetoableChange(propertyName, oldValue, newValue);

        return this;
    }

    /**
     * Delegates to {@link VetoableChangeSupport#fireVetoableChange(String, int, int)}.
     *
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/beans/VetoableChangeSupport.html">VetoableChangeSupport</a>
     */
    protected SerializableBean fireVetoableChange (String propertyName, int oldValue, int newValue)
        throws PropertyVetoException {

        vetoableChangeSupport.fireVetoableChange(propertyName, oldValue, newValue);

        return this;
    }

    /**
     * Delegates to {@link VetoableChangeSupport#fireVetoableChange(String, int, int)}.
     *
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/beans/VetoableChangeSupport.html">VetoableChangeSupport</a>
     */
    protected SerializableBean fireVetoableChange (PropertyChangeEvent propertyChangeEvent)
        throws PropertyVetoException {

        vetoableChangeSupport.fireVetoableChange(propertyChangeEvent);

        return this;
    }

    @Transient
    public List<AggregatePropertyChangeListener<SerializableBean>> getAggregatePropertyChangeListeners() {
        return aggregatePropertyChangeListeners;
    }

    public void addAggregatePropertyChangeListener (
        AggregatePropertyChangeListener<SerializableBean> aggregatePropertyChangeListener) {
        aggregatePropertyChangeListeners.add(aggregatePropertyChangeListener);
    }

    public boolean removeAggregatePropertyChangeListener (
        AggregatePropertyChangeListener<? extends SerializableBean> aggregatePropertyChangeListener) {
        return aggregatePropertyChangeListeners.remove(aggregatePropertyChangeListener);
    }

    /**
     * Method invokes the {@link AggregatePropertyChangeListener#onAggregatePropertyChangeEvent(AggregatePropertyChangeEvent)}
     * method for all {@link AggregatePropertyChangeListener}s that have registered for update notifications.
     */
    public SerializableBean fireAggregatePropertyChangeEvent (
        AggregatePropertyChangeEvent<SerializableBean> aggregatePropertyChangeEvent
    ) {

        for (AggregatePropertyChangeListener<SerializableBean> aggregatePropertyChangeListener : aggregatePropertyChangeListeners)
            aggregatePropertyChangeListener.onAggregatePropertyChangeEvent(aggregatePropertyChangeEvent);

        return this;
    }

    /**
     * Returns the result of the call to EqualsBuilder#reflectionEquals(this, target, CREATED_TIME_MILLIS);
     *
     * @see {@link java.lang.Object#equals(Object)}
     */
    @Override
    public boolean equals(Object target) {
        return EqualsBuilder.reflectionEquals(this, target, CREATED_TIME_MILLIS);
    }

    /**
     * Returns the result of the call to HashCodeBuilder#reflectionHashCode(this, false);
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, false);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

//    Equality test will fail if we leave this.
//    @Transient
//    protected final DefaultTraversalStrategy defaultTraversalStrategy = new DefaultTraversalStrategy ();

    @Override
    public void accept(
        TraversalStrategySpecification traversalStrategy,
        Consumer<SerializableBean>... visitors
    ) {
        accept (traversalStrategy, Arrays.asList(visitors));
    }

    /**
     * @todo Can we do this without the case?
     */
    @Override
    public void accept(
        TraversalStrategySpecification traversalStrategy,
        Collection<Consumer<SerializableBean>> visitors
    ) {
        traversalStrategy.execute(this, visitors);
    }

    @Override
    public void accept(Consumer<SerializableBean>... visitors) {
        accept(Arrays.asList(visitors));
    }

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {
        accept (new DefaultTraversalStrategy (), visitors); // TODO: use a static variable, but note transient is
        // causing an NPE at the moment.
    }

    /**
     * Experimental at the moment -- implementation of the command pattern.
     */
    @Override
    public <S extends SerializableBean> void invoke(Consumer<S> consumer) {
        consumer.accept((S) this);
    }

//    extends Observable<PropertyChangeEvent>
//    
//    @Override
//    protected void subscribeActual(Observer<? super PropertyChangeEvent> observer) {
//
//        final PropertyChangeEventConsumer propertyChangeEventConsumer = new PropertyChangeEventConsumer(observer);
//
//        observer.onSubscribe(propertyChangeEventConsumer);
//
//        addPropertyChangeListener(propertyChangeEventConsumer);
//    }
//
//    static final class PropertyChangeEventConsumer implements PropertyChangeListener, Disposable {
//
//        private final Observer<? super PropertyChangeEvent> observer;
//
//        public PropertyChangeEventConsumer(Observer<? super PropertyChangeEvent> observer) {
//            this.observer = observer;
//        }
//
//        @Override
//        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
//            observer.onNext(propertyChangeEvent);
//        }
//
//        @Override
//        public void dispose() {
//            
//        }
//
//        @Override
//        public boolean isDisposed() {
//            return false;
//        }
//
//        @Override
//        public String toString() {
//            return "PropertyChangeEventConsumer [observer=" + observer + "]";
//        }
//    }
}
