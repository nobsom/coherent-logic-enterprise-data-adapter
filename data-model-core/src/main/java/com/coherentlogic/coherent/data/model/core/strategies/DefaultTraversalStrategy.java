package com.coherentlogic.coherent.data.model.core.strategies;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * Strategy that applies all visitors to the serializableBean.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class DefaultTraversalStrategy implements TraversalStrategySpecification {

    private static final long serialVersionUID = 7655583041832934903L;

    @Override
    public void execute(SerializableBean serializableBean, Collection<Consumer<SerializableBean>> visitors) {

        Optional.of(visitors)
            .ifPresent(
                values -> {
                    values.forEach(visitor -> { visitor.accept(serializableBean); });
                }
            );
    }
}
