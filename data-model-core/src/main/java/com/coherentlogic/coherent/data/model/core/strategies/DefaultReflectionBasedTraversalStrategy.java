package com.coherentlogic.coherent.data.model.core.strategies;

import java.util.Collection;
import java.util.function.Consumer;

import com.coherentlogic.coherent.data.model.core.annotations.VisitableAnnotationProcessor;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * @deprecated If this class is no longer necessary, delete it.
 */
public class DefaultReflectionBasedTraversalStrategy implements TraversalStrategySpecification {

    private final VisitableAnnotationProcessor visitableAnnotationProcessor;

    public DefaultReflectionBasedTraversalStrategy () {
        this ( new VisitableAnnotationProcessor () );
    }

    public DefaultReflectionBasedTraversalStrategy (VisitableAnnotationProcessor visitableAnnotationProcessor) {
        this.visitableAnnotationProcessor = visitableAnnotationProcessor;
    }

    @Override
    public void execute(SerializableBean serializableBean, Collection<Consumer<SerializableBean>> visitors) {
    }

//    @Override
//    public void execute(SerializableBean serializableBean, Collection<Consumer<SerializableBean>> visitors) {
//        Set<Field> results = visitableAnnotationProcessor.getFieldsFor(serializableBean.getClass());
//        
//        results.forEach(
//            field -> {
//                if (field.get(serializableBean) instanceof Collection) {
//                    
//                }
//            }
//        );
//        
//    }
}
