package com.coherentlogic.coherent.data.model.core.domain;

/**
 * A specification for domain classes which have an id.
 *
 * @param <I> The type of id.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public interface IdentitySpecification<I> {

    static final String ID = "id";

    void setId (I id);

    I getId ();
}
