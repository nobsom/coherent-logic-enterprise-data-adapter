package com.coherentlogic.coherent.data.model.core.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import com.coherentlogic.coherent.data.model.core.strategies.TraversalStrategySpecification;

/**
 * A specification for beans that implement the visitor pattern.
 *
 * @param <T> The type of data being consumed by the visitor.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public interface AcceptorSpecification extends Serializable {

    void accept (
        TraversalStrategySpecification traversalStrategy,
        Consumer<SerializableBean>... visitors
    );

    void accept (
        TraversalStrategySpecification traversalStrategy,
        Collection<Consumer<SerializableBean>> visitors
    );

    void accept (Consumer<SerializableBean>... visitors);

    void accept (Collection<Consumer<SerializableBean>> visitors);
}
