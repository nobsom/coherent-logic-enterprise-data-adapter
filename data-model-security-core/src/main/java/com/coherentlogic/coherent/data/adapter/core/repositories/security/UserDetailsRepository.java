package com.coherentlogic.coherent.data.adapter.core.repositories.security;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.coherentlogic.coherent.data.model.core.domain.security.UserDetailsBean;

@Transactional
public interface UserDetailsRepository extends JpaRepository<UserDetailsBean, Long> {

    UserDetailsBean findByUsername (String username);
}
