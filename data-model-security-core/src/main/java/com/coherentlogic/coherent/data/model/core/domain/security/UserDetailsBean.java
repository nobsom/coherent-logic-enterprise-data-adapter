package com.coherentlogic.coherent.data.model.core.domain.security;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.security.core.userdetails.UserDetails;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.domain.IdentitySpecification;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * 
 * @author thospfuller
 *
 */
@Entity
@Table(name = UserDetailsBean.USER_DETAILS_TABLE)
public class UserDetailsBean extends SerializableBean implements UserDetails, IdentitySpecification<Long> {

    private static final long serialVersionUID = -6487967805577534685L;

    static final String
        AUTHORITIES = "authorities",
        USERNAME = "username",
        PASSWORD = "password",
        ACCOUNT_NON_EXPIRED = "accountNonExpired",
        ACCOUNT_NON_LOCKED = "accountNonLocked",
        CREDENTIALS_NON_EXPIRED = "credentialsNonExpired",
        ENABLED = "enabled",
        USER_DETAILS_TABLE = "userDetails";

    private Long id;

    private String username, password;

    private boolean accountNonExpired, accountNonLocked, credentialsNonExpired, enabled;

    private List<GrantedAuthorityBean> authoritiesList;

    static final String
        USER_DETAILS_GRANTED_AUTHORITIES = "user_details_granted_authorities",
        USER_DETAILS_KEY = "user_details_key",
        GRANTED_AUTHORITIES_KEY = "granted_authorities_key";

    @Override
    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable( 
        name = USER_DETAILS_GRANTED_AUTHORITIES,
        joinColumns = @JoinColumn(
            name = USER_DETAILS_KEY,
            referencedColumnName = PRIMARY_KEY
        ), 
        inverseJoinColumns = @JoinColumn(
            name = GRANTED_AUTHORITIES_KEY,
            referencedColumnName = PRIMARY_KEY
        )
    )
    public List<GrantedAuthorityBean> getAuthorities() {
        return authoritiesList;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setAuthorities(
        @Changeable (AUTHORITIES) List<GrantedAuthorityBean> authoritiesList
    ) {
        this.authoritiesList = authoritiesList;
    }

    public void setUsername(@Changeable (USERNAME) String username) {
        this.username = username;
    }

    public void setPassword(@Changeable (PASSWORD) String password) {
        this.password = password;
    }

    public void setAccountNonExpired(@Changeable (ACCOUNT_NON_EXPIRED) boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public void setAccountNonLocked(@Changeable (ACCOUNT_NON_LOCKED) boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public void setCredentialsNonExpired(
        @Changeable (CREDENTIALS_NON_EXPIRED) boolean credentialsNonExpired
    ) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public void setEnabled(@Changeable (ENABLED) boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public void setId(@Changeable (ID) Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (accountNonExpired ? 1231 : 1237);
        result = prime * result + (accountNonLocked ? 1231 : 1237);
        result = prime * result + ((authoritiesList == null) ? 0 : authoritiesList.hashCode());
        result = prime * result + (credentialsNonExpired ? 1231 : 1237);
        result = prime * result + (enabled ? 1231 : 1237);
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserDetailsBean other = (UserDetailsBean) obj;
        if (accountNonExpired != other.accountNonExpired)
            return false;
        if (accountNonLocked != other.accountNonLocked)
            return false;
        if (authoritiesList == null) {
            if (other.authoritiesList != null)
                return false;
        } else if (!authoritiesList.equals(other.authoritiesList))
            return false;
        if (credentialsNonExpired != other.credentialsNonExpired)
            return false;
        if (enabled != other.enabled)
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        if (username == null) {
            if (other.username != null)
                return false;
        } else if (!username.equals(other.username))
            return false;
        return true;
    }

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        visitors.forEach(
            visitor -> {
                visitor.accept(this);
            }
        );

        getAuthorities().forEach(
            authority -> {
                ((GrantedAuthorityBean) authority).accept(visitors);
            }
        );
    }

    @Override
    public String toString() {
        return "UserDetailsBean [username=" + username + ", password=" + password + ", accountNonExpired="
            + accountNonExpired + ", accountNonLocked=" + accountNonLocked + ", credentialsNonExpired="
            + credentialsNonExpired + ", enabled=" + enabled + ", authoritiesList=" + authoritiesList +
            ", id=" + id + "]";
    }
}
