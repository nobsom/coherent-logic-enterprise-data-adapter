package com.coherentlogic.coherent.data.adapter.core.builders.rest.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.function.BiFunction;
import java.util.function.Function;

import javax.ws.rs.core.UriBuilder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.xquery.XQConnection;
import javax.xml.xquery.XQDataSource;
import javax.xml.xquery.XQException;
import javax.xml.xquery.XQExpression;
import javax.xml.xquery.XQPreparedExpression;
import javax.xml.xquery.XQResultSequence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.coherentlogic.coherent.data.adapter.core.builders.rest.AbstractRESTQueryBuilder;
import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.command.CommandExecutorSpecification;
//import com.coherentlogic.coherent.data.adapter.core.builders.rest.Configuration;
//import com.coherentlogic.coherent.data.adapter.core.builders.rest.SaxonXQDataSource;
//import com.coherentlogic.coherent.data.adapter.core.builders.rest.XQConnection;
//import com.coherentlogic.coherent.data.adapter.core.builders.rest.XQDataSource;
//import com.coherentlogic.coherent.data.adapter.core.builders.rest.XQException;
//import com.coherentlogic.coherent.data.adapter.core.builders.rest.XQExpression;
//import com.coherentlogic.coherent.data.adapter.core.builders.rest.XQPreparedExpression;
//import com.coherentlogic.coherent.data.adapter.core.builders.rest.XQResultSequence;
//import com.coherentlogic.coherent.data.adapter.core.builders.rest.json.AbstractGSONQueryBuilder;
import com.coherentlogic.coherent.data.adapter.core.exceptions.ConversionFailedException;
import com.coherentlogic.coherent.data.adapter.core.exceptions.GenericRuntimeException;
import com.coherentlogic.coherent.data.adapter.core.strategies.xml.DOMTraversalStrategySpecification;

import net.sf.saxon.Configuration;
import net.sf.saxon.xqj.SaxonXQDataSource;

public abstract class AbstractXMLQueryBuilder<K> extends AbstractRESTQueryBuilder<K> {

    private static final Logger log = LoggerFactory.getLogger(AbstractXMLQueryBuilder.class);

    public AbstractXMLQueryBuilder(RestTemplate restTemplate, String uri, CacheServiceProviderSpecification<K> cache,
        CommandExecutorSpecification<K> commandExecutor) {
        super(restTemplate, uri, cache, commandExecutor);
    }

    public AbstractXMLQueryBuilder(RestTemplate restTemplate, String uri, CacheServiceProviderSpecification<K> cache) {
        super(restTemplate, uri, cache);
    }

    public AbstractXMLQueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CommandExecutorSpecification<K> commandExecutor
    ) {
        super(restTemplate, uri, commandExecutor);
    }

    public AbstractXMLQueryBuilder(RestTemplate restTemplate, String uri) {
        super(restTemplate, uri);
    }

    public AbstractXMLQueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<K> cache,
        CommandExecutorSpecification<K> commandExecutor
    ) {
        super(restTemplate, uriBuilder, cache, commandExecutor);
    }

    public AbstractXMLQueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<K> cache
    ) {
        super(restTemplate, uriBuilder, cache);
    }

    public AbstractXMLQueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CommandExecutorSpecification<K> commandExecutor
    ) {
        super(restTemplate, uriBuilder, commandExecutor);
    }

    public AbstractXMLQueryBuilder(RestTemplate restTemplate, UriBuilder uriBuilder) {
        super(restTemplate, uriBuilder);
    }

    /**
     * Method calls the web service and converts the resultant XML String into an instance of {@link Document} and
     * returns this document.
     *
     * @todo Possibly move this to a child class (TBD).
     */
    public Document doGetAsXMLDocument () {
        return (Document) doGetAsXMLDocument ((Document document) -> { return document; });
    }

    /**
     * Method calls the web service and converts the resultant XML String into an instance of {@link Document}, applies
     * the function to the document, and returns the result as an object of type R.
     *
     * @todo Possibly move this to a child class (TBD).
     */
    public <R> R doGetAsXMLDocument (Function<Document, R> function) {

        String xml = doGetAsString();

        return doGetAsXMLDocument (xml, function);
    }

    public <R> R doGetAsXMLDocument (String xml, Function<Document, R> function) {
        return doGetAsXMLDocument (xml, DocumentBuilderFactory.newInstance(), function);
    }

    public DOMTraversalStrategySpecification depthFirstDOMTraversalStrategy = new DOMTraversalStrategySpecification () {

        @Override
        public <R> R execute(Node node, Function<Node, R> function) {

            if (node.hasChildNodes()) {

                NodeList nodeList = node.getChildNodes();

                for (int index = 0; index < nodeList.getLength(); index++) {

                    Node childNode = nodeList.item(index);

                    execute (childNode, function);
                }
            }

            return function.apply(node);
        }
    };

    /**
     * Untested.
     */
    public DOMTraversalStrategySpecification breadthFirstDOMTraversalStrategy = new DOMTraversalStrategySpecification () {

        @Override
        public <R> R execute(Node node, Function<Node, R> function) {

            R result = function.apply(node);

            if (node.hasChildNodes()) {

                NodeList nodeList = node.getChildNodes();

                for (int index = 0; index < nodeList.getLength(); index++) {

                    Node childNode = nodeList.item(index);

                    execute (childNode, function);
                }
            }

            return result;
        }
    };

    /**
     * Invokes the {@link #doGetAsXMLDocumentAndTraverse(DOMTraversalStrategySpecification, Function)} using the
     * {@link #depthFirstDOMTraversalStrategy}.
     *
     * @param function Takes a {@link Node} parameter and returns a result of type R.
     *
     * @return A result of type R.
     */
    public <R> R doGetAsXMLDocumentAndTraverse (Function<Node, R> function) {
        return doGetAsXMLDocumentAndTraverse(depthFirstDOMTraversalStrategy, function);
    }

    /**
     * Traverses every node in the resultant {@link Document}.
     *
     * @param traversalStrategy One of {@link #depthFirstDOMTraversalStrategy},
     *  {@link #breadthFirstDOMTraversalStrategy}, or provide a custom traversal strategy.
     *
     * @param function A function which takes a {@link Node} parameter and returns a result of type R.
     *
     * @return A result of type R.
     */
    public <R> R doGetAsXMLDocumentAndTraverse (
        DOMTraversalStrategySpecification traversalStrategy,
        Function<Node, R> function
    ) {
        Document document = doGetAsXMLDocument ();

        return traversalStrategy.execute(document, function);
    }

    /**
     * Method gets the XML as an instance of DOM {@link Document} and then invokes the biFunction passing the
     * transformer and the node and returning a result of type R.
     *
     * @param tranformer Used to convert the resultant {@link Document} into a result of type R.
     *
     * @param biFunction Contains custom logic that is applied to the Document, which uses the Transformer, and which
     *  returns a result of type R.
     *
     * @return A result of type R.
     *
     * @todo Consider making transformer generic and just passing anything to the biFunction.
     */
    public <R> R doGetAsXMLDocumentAndTransform (Transformer transformer, BiFunction<Node, Transformer, R> biFunction) {

        Document document = doGetAsXMLDocument ();

        return biFunction.apply(document, transformer);
    }

    /**
     * Method calls the web service and converts the resultant XML String into an instance of {@link Document}, applies
     * the function to the document, and returns the result as an object of type R.
     *
     * @todo Possibly move this to a child class (TBD).
     */
    public <R> R doGetAsXMLDocument (
        String xml, DocumentBuilderFactory documentBuilderFactory, Function<Document, R> function) {

        if (log.isDebugEnabled())
            log.debug("xml: " + xml);

        Document result = null;

        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();  

            result = documentBuilder.parse(
                new InputSource(
                    new StringReader(xml)
                )
            );

        } catch (ParserConfigurationException | SAXException | IOException cause) {
            throw new ConversionFailedException("Unable to convert the xml into an instance of Document.", cause);
        }

        return function.apply(result);
    }

    <R> R doGetAsXMLDocument (
        String xml, DocumentBuilderFactory documentBuilderFactory, String xQueryStatement
    ) {
        throw new RuntimeException ("Method not implemented.");
    }

    /**
     * 
     * See also: https://github.com/spring-projects/spring-integration-extensions/tree/master/spring-integration-xquery
     * 
     * http://www.saxonica.com/documentation/index.html
     * 
     * @param xml
     * @param documentBuilderFactory
     * @param xqExpression
     * @return
     */
    <R> R doGetAsXQPreparedExpression (
        String xml,
        DocumentBuilderFactory documentBuilderFactory,
        XQExpression xqExpression
    ) {

        XQDataSource xqDataSource = new SaxonXQDataSource ();

        XQConnection xqConnection = null;

        try {
            xqConnection = xqDataSource.getConnection();
        } catch (XQException xqException) {
            throw new GenericRuntimeException("Unable to get the connection.", xqException);
        }

        InputStream inputStream = new ByteArrayInputStream(xml.getBytes());

        XQPreparedExpression xqPreparedExpression = null;

        // See Example 6 here: https://www.programcreek.com/java-api-examples/?class=net.sf.saxon.xqj.SaxonXQDataSource&method=getConnection
        try {
            xqPreparedExpression = xqConnection.prepareExpression(inputStream);
        } catch (XQException xqException) {
            throw new GenericRuntimeException("Unable to prepare the expression.", xqException);
        }

        XQResultSequence result = null;

        try {
            result = xqPreparedExpression.executeQuery();
        } catch (XQException xqException) {
            throw new GenericRuntimeException("Unable to execute the query.", xqException);
        }

        try {
            xqConnection.close();
        } catch (XQException xqException) {
            throw new GenericRuntimeException("Unable to close the connection.", xqException);
        }

        return null;//result;
    }

    /**
     * Experimental
     */
    public XQDataSource newXQDataSource () {
        return new SaxonXQDataSource ();
    }

    /**
     * Experimental
     */
    public XQDataSource newXQDataSource (Configuration config) {
        return new SaxonXQDataSource (config);
    }
}
