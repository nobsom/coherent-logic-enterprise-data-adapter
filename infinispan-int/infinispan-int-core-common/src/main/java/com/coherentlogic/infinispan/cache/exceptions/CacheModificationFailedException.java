package com.coherentlogic.infinispan.cache.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * Thrown when the put operation on the cache has failed; note the parent exception will contain the details
 * regarding what went wrong.
 */
public class CacheModificationFailedException extends NestedRuntimeException {

    private static final long serialVersionUID = -4691394035050864970L;

    public CacheModificationFailedException(String msg) {
        super(msg);
    }

    public CacheModificationFailedException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
