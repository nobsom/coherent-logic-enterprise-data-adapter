package com.coherentlogic.infinispan.cache.providers;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;

import org.infinispan.AdvancedCache;
import org.infinispan.Cache;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.util.concurrent.locks.LockManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.infinispan.cache.exceptions.CacheModificationFailedException;
import com.coherentlogic.infinispan.cache.exceptions.InfinispanMisconfigurationRuntimeException;
import com.coherentlogic.infinispan.cache.exceptions.LockNotAcquiredException;
import com.coherentlogic.infinispan.cache.exceptions.TransactionRollbackFailedException;

public class DefaultInfinispanCacheServiceProvider
    implements CacheServiceProviderSpecification<String> {

    private static final Logger log =
        LoggerFactory.getLogger(DefaultInfinispanCacheServiceProvider.class);

    private final Map<String, SerializableBean> cache;

//    private final ClusteredLockManager clusteredLockManager;

    public DefaultInfinispanCacheServiceProvider(Map<String, SerializableBean> cache) {

        this.cache = cache;

        AdvancedCache<String, SerializableBean> advancedCache =
            ((Cache<String, SerializableBean>)cache).getAdvancedCache();

        EmbeddedCacheManager cacheManager = advancedCache.getCacheManager();

        // See <a href="http://infinispan.org/docs/stable/user_guide/user_guide.html#clustered_lock">
        // 13.3. The ClusteredLockManager interface.</a>
//        clusteredLockManager = EmbeddedClusteredLockManagerFactory.from(cacheManager);

        TransactionManager transactionManager = advancedCache.getTransactionManager();

        if (transactionManager == null)
            throw new InfinispanMisconfigurationRuntimeException ("The Infinispan transactionManager "
                + "cannot be null.");
    }

    @Override
    public <V extends SerializableBean> V get(String key) {
        return (V) cache.get(key);
    }

    @Override
    public <V extends SerializableBean> void put(String key, V value) {

        log.debug("put: method begins; key: " + key + ", value: " + value);

        AdvancedCache<String, SerializableBean> advancedCache =
            ((Cache<String, SerializableBean>)cache).getAdvancedCache();

        try {

            TransactionManager transactionManager = advancedCache.getTransactionManager();

            transactionManager.begin();

            lock(advancedCache, key);

            SerializableBean oldValue = cache.put(key, value);

            log.debug("put returned for key: " + key + " with oldValue: " + oldValue);

            unlock(advancedCache, key);

            advancedCache.getTransactionManager().commit();

        } catch (
            SecurityException
            | IllegalStateException
            | NotSupportedException
            | SystemException
            | RollbackException
            | HeuristicMixedException
            | HeuristicRollbackException exception
        ) {

            try {
                advancedCache.getTransactionManager().rollback();
            } catch (IllegalStateException | SecurityException | SystemException rollbackException) {
                throw new TransactionRollbackFailedException ("Unable to *put* the key: " + key + " and value: " +
                    value + " into the cache " + cache, rollbackException, exception);
            }

            throw new CacheModificationFailedException ("Unable to *put* the key: " + key + " and value: " +
                value + " into the cache " + cache, exception);
        }

        log.debug("put: method ends for key: " + key + " and value: " + value);
    }

    /**
     * Method locks the cache for the specified keys.
     */
    void lock (AdvancedCache<String, SerializableBean> advancedCache, String... keys) {

        boolean locked = advancedCache.lock(keys);

        if (!locked)
            throw new LockNotAcquiredException("Unable to obtain a lock on the key: " + keys);
        else {

            StringBuffer buffer = new StringBuffer ("A lock has been acquired on the following keys: ");

            for (String key : keys)
                buffer.append(key).append(",");

            int lastComma = buffer.lastIndexOf(",");

            String message = buffer.substring(0, lastComma);

            log.debug(message);
        }
    }

    /**
     * Method unlocks the cache for the specified keys.
     */
    void unlock (AdvancedCache<String, SerializableBean> advancedCache, String... keys) {

        LockManager lockManager = advancedCache.getLockManager();

        Object lockOwner = lockManager.getOwner(keys);

        Collection<Object> keysCollection = Collections.<Object>singletonList(keys);

        lockManager.unlock(keysCollection, lockOwner);
    }

    @Override
    public String toString() {
        return "DefaultInfinispanCacheServiceProvider [cache=" + cache + "]";
    }
}
